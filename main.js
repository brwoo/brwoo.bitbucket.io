const $ = (selector, all=false) => {
  let out = [...document.querySelectorAll(selector)];
  return out.length > 1 || all ? out : out[0];
}

const linkify = input => input
    .replace(/(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim, v => 
      `<a href="${v}" target="_blank">${v.length > 50 ? v.slice(0, 50) + "..." : v}</a>`
    )
    .replace(/(^|[^\/])(www\.[\S]+(\b|$))/gim, '$1<a href="http://$2" target="_blank">$2</a>')
    .replace(/(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim, '<a href="mailto:$1">$1</a>');


const error = err => {
  $(".error").innerText = err;
  $(".error").classList.remove("hidden");
  setTimeout(_ => {
    $(".error").classList.add("hidden");
  }, 3000);
}

const API = "https://brwoo.herokuapp.com/api/v1?";

let results, count;
let page = 0;
document.querySelector("#searchform").onsubmit = async e => {
  e.preventDefault();
  page = 0;
  count = 0;
  await search($("#search").value);
}

const search = async input => {
  if(input.length > 0 && input.length < 100 && input.match(/^[a-zA-Zа-яА-Яё0-9 ]*$/g)) {
    $(".jumbotron").classList.add("minimized");
    try{
      $(".loading").classList.remove("hidden");
      let request = `${API}q=${input}`;
      results = await (await fetch(`${request}&sk=${page*10}`)).json();
      count = parseInt(await (await fetch(`${request}&c=true`)).text());
    }
    catch(err){
      error(err);
    }
    finally {
      updatePageNavigator();
      $(".results").classList.remove("hidden");
      $(".loading").classList.add("hidden");
      scroll(0,0);
    }
    $(".results .container").innerHTML="";
    if(results.length > 0){
      results.forEach((v, i) => {
        $(".results .container").innerHTML += getResultItem(
          i,
          `${v.filename}.jpg`,
          getTime(v.duration),
          v.title,
          v.description.split("\n").slice(0,3).join("\n"),
          parseSubtitles(v.subtitles, input)
            .map(subtitle=>getSubtitleHtml(subtitle, v.url))
            .reduce((html, subtitle) => html+=subtitle)
        );
      })
      console.log(results);
    }
    else $(".results .container").innerHTML="Ничего не найдено";
  }
  else {
    error("Фраза может содержать только буквы и цифры и должна быть не длиннее 100 символов")
  }
}

const getResultItem = (id, image, time, title, description, subtitles) => {
  return `
  <div class="item">
    <div class="image">
      <div class="cover" style="background-image: url('images/${image.replace("'", "\\'")}')">
        <div class="duration">${time}</div>
      </div>
    </div>
    <div class="text">
      <div class="title">${title}</div>
      <div class="description">${linkify(description)}</div>
      <div class="subtitles">${subtitles}</div>
      <div class="buttons">
        <button onclick="openAbout(${id})">Подробнее</button>
      </div>
    </div>
  </div>
  `;
}

const getTime = duration => `${Math.floor(duration/60)}:${duration%60}`;

getNormalizedSubtitles = subtitle => {
  let subtitlesUnsorted = subtitle.split("\n").filter(v=>v);
  let subtitles = [];
  for(let i=0; i<subtitlesUnsorted.length-3; i+=3) {
    subtitles.push({
      time: subtitlesUnsorted[i+1],
      text: subtitlesUnsorted[i+2]
    });
  }
  return subtitles;
}

const parseSubtitles = (subtitle, input) => {
  return getNormalizedSubtitles(subtitle)
    .filter(subtitle=>subtitle.text.toLowerCase().indexOf(input.toLowerCase())!==-1)
    .map(subtitle=>({
      text: subtitle.text.replace(new RegExp(`(${input})`, "ig"), `<span class="highlight">$1</span>`),
      time: subtitle.time
    }));
}

getSubtitleHtml = (subtitle, url="#") => {
  return `
  <div class="subtitle">
    <div class="subtime">
      <a href="${url}&t=${getYoutubeTime(subtitle.time)}" target="blank">
        ${subtitle.time.replace(/\.(.*?)( |$)/g, "$2")}
      </a>
    </div>
    <div class="subtext">${subtitle.text}</div>
  </div>
  `;
}

const getYoutubeTime = time => {
  let separators = ["s", "m", "h"];
  return time
    .replace(/00:|((\.| -->)(.*))/g, "")
    .replace("00","0").split(":")
    .reverse()
    .map((e,i) => e+separators[i])
    .reverse()
    .join("")
}



const changePage = direction => {
  if(direction > 0) {
    page++;
  }
  else page--;
  search($("#search").value);
}

const updatePageNavigator = _ => {
  if(count > 0) {
    $(".page-navigator").style.visibility = "visible";
    if(page===0)  $(".page-navigator .prev").classList.add("hidden")
      else $(".page-navigator .prev").classList.remove("hidden");
    if(page*10+10 > count)  $(".page-navigator .next").classList.add("hidden")
      else $(".page-navigator .next").classList.remove("hidden");
    pages = Math.floor(count/10);
    pages = count%10 > 0 ? pages+1 : pages;
    $(".page-navigator .count").innerHTML = `${page+1} / ${pages}`;
  }
  else $(".page-navigator").style.visibility = "hidden";
}

const generateAbout = (image, uploader, tags, title, url, description, subtitles, comments="Комментарии отсутствуют") => {
  return `
    <div class="baseline">
      <div class="left">
        <div class="image" style="background-image:url('images/${image.replace("'", "\\'")}')"></div>
        <div class="author">${uploader}</div>
        <div class="tags">${tags}</div>
      </div>
      <div class="right">
        <div class="title">${title} <a href="${url}" target="_blank"><img src="images/yt.svg" alt="youtube"></a></div>
        <div class="description">${linkify(description)}</div>
      </div>
    </div>
    <div class="lower-line">
      <div class="subtitles">${subtitles}</div>
      <div class="comments">${comments}</div>
    </div>
    <div class="close" onclick="closeAbout()">Закрыть</div>
  `;
}

const openAbout = id => {
  scroll(0,0);
  $(".about").classList.remove("hidden");
  $("body").style.overflow="hidden";
  let res = results[id];
  $(".about").innerHTML = generateAbout(
    `${res.filename}.jpg`,
    res.uploader,
    res.tags.map(v=>`<div class="tag">${v}</div>`).join(""),
    res.title,
    res.url,
    res.description,
    getNormalizedSubtitles(res.subtitles)
      .map(v=>getSubtitleHtml(v,res.url))
      .join(""),
    commentsHtml(res.comments)
  );
}

const closeAbout = _ => {
  $(".about").classList.add("hidden")
  $(".about").innerHTML = "";
  $("body").style.overflow="visible";
}

const commentHtml = c => {
  const hashCode = str => {
      let hash = 0;
      for (let i = 0; i < str.length; i++) {
         hash = str.charCodeAt(i) + ((hash << 5) - hash);
      }
      return hash;
  } 
  const intToRGB = i => {
      let c = (i & 0x00FFFFFF)
          .toString(16)
          .toUpperCase();
      return "#00000".substring(0, 7 - c.length) + c;
  }

  return `
  <div class="comment">
    <div class="info">
      <div class="avatar" style="background-color:${intToRGB(hashCode(c.authorLink.replace(/\/user\/|\/channel\/|\//g, "")))}">
        ${c.author.slice(0,1).toUpperCase()}
      </div>
      <div class="text">
        <div class="name"><a href="https://www.youtube.com${c.authorLink}" target="_blank">${c.author}</a></div>
        <div class="meta">
          <div class="date">
            ${(new Date(c.timestamp)).toLocaleDateString("ru-RU")}
          </div>
          <div class="likes">${c.likes}</div>
          <div class="edited">
            ${c.edited ? "изменено" : ""}
          </div>
        </div>
      </div>
    </div>
    <div class="message">
      ${c.text}
    </div>
    <div class="replies">
      ${c.replies.map(e => commentHtml(e)).join("")}
    </div>
  </div>
  `;
}

const commentsHtml = c => (c||[])
  .map(v=>commentHtml(v)).join("");